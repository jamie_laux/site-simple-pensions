js_pipeline  = require 'js-pipeline'
css_pipeline = require 'css-pipeline'
contentful = require 'roots-contentful'
slugify = require 'slugify'
dynamic = require 'dynamic-content'
marked = require 'marked'

module.exports =
  ignores: ['readme.md', '**/layout.*', '**/_*', '.gitignore', 'ship.*conf']

  locals:
    markdown: marked

  extensions: [
    js_pipeline(files: 'assets/js/*.coffee'),
    css_pipeline(files: 'assets/css/*.css'),

    contentful
      access_token: 'f964543d00a5d27b5bc02043fed91880d84f29f23fbd95d4dc124bbc8781a272'
      space_id: '6pf63u9k71p2'
      content_types: [
        id: '2wKn6yEnZewu2SCCkus4as'
        template: 'views/question.jade'
        path: (e) -> "/questions/#{slugify(e.title)}",

        id: 'pVjIutw41wsq00QeOoqAO'
        template: 'views/segment.jade'
        path: (e) -> "/segments/#{slugify(e.seo)}"
      ]
          
  ]

  'coffee-script':
    sourcemap: true

  jade:
    pretty: true